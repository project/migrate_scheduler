# Migrate Scheduler

The Migrate Scheduler module provides the functionality of executing the
migrations on a particular schedule.

Cron API which is built into the Drupal core is used to schedule the migrations.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/migrate_scheduler).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/migrate_scheduler).


## Contents of this file

- Requirements
- Recommended modules
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Recommended modules

- [Migrate Plus](https://www.drupal.org/project/migrate_plus): Enhancements to
  core migration support.
- [Migrate Tools](https://www.drupal.org/project/migrate_tools): Tools to assist
  in developing and running migrations.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module currently relies on the configuration set in the settings.php file,
with a plan to support the configuration from the admin UI.

Place the following configuration variable in any of the active settings.php, or
settings.local.php, or settings.local.php

```
$config['migrate_scheduler']['migrations'] = [
  'migration_1' => [
    'time' => 3600,  # To be executed after every 1 hour.
    'update' => TRUE,  # To be executed with the --update flag.
  ],
  'migration_2' => [
    'time' => 28800, # To be executed after every 8 hours.
  ],
  'migration_3' => [
    'time' => 60, # To be executed every minute.
    'sync' => TRUE, # To be executed with the --sync flag.
  ]
];
```

The above configuration is similar to executing:

- `drush mim migration_1 --update` - after every hour.
- `drush mim migration_2` - after every 8 hours.
- `drush mim migration_3 --sync` - after every one minute.


## Maintainers

- Ajit Shinde - [AjitS](https://www.drupal.org/u/ajits)

This project has been sponsored by:

- CoLab Coop: CoLab is a worker-owned digital agency that started in 2010 in Ithaca,
  New York. Visit: [CoLab Coop](https://colab.coop) for more information.
